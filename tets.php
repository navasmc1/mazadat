<?php
echo ("http://www.elated.com/articles/php-anonymous-functions/");
echo ("<br/>");
echo ("Session fact Example.");
echo ("<br/>");
echo ("________________________");
echo ("<br/>");
session_start();
$_SESSION['name'] = 'NAVAS';
$_SESSION['kil'] = 'Used';
session_destroy();
var_dump(count($_SESSION));
echo ("<br/>");
echo ("Call back function Example.");
echo ("<br/>");
echo ("________________________");
echo ("<br/>");
/*Call back*/
function string($name,$callback){
	$result=array(
		'upper'=>strtoupper($name),
		'lower'=>strtolower($name)
	);
	if(is_callable($callback)){
		call_user_func($callback,$result);
	}
	
}
string("James",function($name){
	
	var_dump($name);
	
});
echo ("<br/>");
echo ("Anonimous function Example.");
echo ("<br/>");
echo ("________________________");
echo ("<br/>");
/*anonimous*/
//USE-1
/*assign to variable*/
$makeGreeting = function($name,$timeOfDay){
	return ( "Good $timeOfDay, $name!" );
};
echo $makeGreeting( "Fred", "morning" ) . "<br>";
echo $makeGreeting( "Mary", "afternoon" ) . "<br>";
//USE-2
/*store to arry*/
$luckyDip = array(
			function(){
				 echo "You got a bag of 1 toffees!";
			},function(){
				 echo "You got a bag of 2 toffees!";
			},function(){
				 echo "You got a bag of 3 toffees!";
			},function(){
				 echo "You got a bag of 4 toffees!";
			}
);
$choice = rand( 0, 3 );
$luckyDip[$choice]();
//USE-3
/*As callback function*/
echo ("<br/>");
echo ("Anonimous function Callback Example.");
echo ("<br/>");
echo ("_________Eg-1 array_map_______________");
echo ("<br/>");

// Create a regular callback function...
function nameToGreeting( $name ) {
  return "Hello " . ucfirst( $name ) . "!";
}
 
// ...then map the callback function to elements in an array.
$names = array( "fred", "mary", "sally" );
echo('<pre>');
print_r( array_map( 'nameToGreeting', $names ) );
echo('</pre>');

echo ("<br/>");
echo ("_________Eg-2 usort_______________");
echo ("<br/>");

$people = array(
  array( "name" => "Fred", "age" => 39 ),
  array( "name" => "Sally", "age" => 23 ),
  array( "name" => "Mary", "age" => 46 )
);
usort( $people, function( $personA, $personB ) {
  return ( $personA["age"] < $personB["age"] ) ? -1 : 1;
} );
 
echo('<pre>');
print_r($people);
echo('</pre>');

echo ("<br/>");
echo ("___Creating closures with anonymous functions________");
echo ("<br/>");

function gogreetings(){
	$timeofDay = 'morning';
	
	return( function( $name ) use ( &$timeofDay ){
		    $timeofDay = ucfirst( $timeofDay ); 
		return ( "Good $timeofDay $name !!" );
	});
	
};

$gotGreetings = gogreetings();

echo($gotGreetings("Fred"));
echo ("<br/>");
echo ("___Using closures to pass additional data to callbacks________");
echo ("<br/>");
function getSortFunction($sortkey){
	
	return (function($personA,$personB) use($sortkey){
		
		return ($personA[$sortkey]>$personB[$sortkey])?1:-1;
		
	});
	
}

$people = array(
  array( "name" => "Fred", "age" => 39 ),
  array( "name" => "Sally", "age" => 23 ),
  array( "name" => "Mary", "age" => 46 )
);
echo('<pre>');
print_r($people);
echo('</pre>');
echo "Sorted by name:<br><br>";
usort( $people, getSortFunction( "name" ) );
echo('<pre>');
print_r($people);
echo('</pre>');
echo "<br>";
echo "Sorted by age:<br><br>";
usort( $people, getSortFunction( "age" ) );
echo('<pre>');
print_r($people);
echo('</pre>');
echo "<br>";