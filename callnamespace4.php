<?php
/*namespace aliasing*/
use App\Lib1 as L;
use App\Lib2\MyClass as Obj;

header('Content-type: text/plain');
require_once('namespace/lib1.php');
require_once('namespace/lib2.php');

echo L\MY_CONT_PATH . "\n";
echo L\myFunction() . "\n";
echo L\MyClass::go() . "\n";

echo Obj::go() . "\n";
?>