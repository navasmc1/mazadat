<?php
header('Content-type: text/plain');
require_once('namespace/lib1.php');
require_once('namespace/lib2.php');
echo \App\Lib1\MY_CONT_PATH . "\n";
echo \App\Lib1\myFunction() . "\n";
echo \App\Lib1\MyClass::go() . "\n";

echo \App\Lib2\MY_CONT_PATH . "\n";
echo \App\Lib2\myFunction() . "\n";
echo \App\Lib2\MyClass::go() . "\n";
?>