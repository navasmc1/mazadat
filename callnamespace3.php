<?php
/*namespace importing*/
use App\Lib1,App\Lib2;
require_once('namespace/lib1.php');
require_once('namespace/lib2.php');
header('Content-type: text/plain');
echo Lib1\MY_CONT_PATH . "\n";
echo Lib1\myFunction() . "\n";
echo Lib1\MyClass::go() . "\n";

echo Lib2\MY_CONT_PATH . "\n";
echo Lib2\myFunction() . "\n";
echo Lib2\MyClass::go() . "\n";
?>